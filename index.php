<?php


require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold Bloded : " . $sheep->Cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold Bloded : " . $kodok->Cold_blooded . "<br>";
$kodok->jump();
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold Bloded : " . $sungokong->Cold_blooded . "<br>";
$sungokong->yell();

?>
